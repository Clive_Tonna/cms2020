-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 14, 2020 at 06:23 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `placesdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(255) NOT NULL,
  `location` varchar(300) NOT NULL,
  `date` date NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `guide` varchar(300) NOT NULL,
  `rating` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `location`, `date`, `price`, `guide`, `rating`) VALUES
(1, 'Marsascala to Marsaxlokk Bay', '2020-05-23', '12', 'Paul Cauchi', 3),
(2, 'Valletta', '2020-05-26', '6', 'Robert Pace', 5),
(3, 'Gozo coastal walk', '2020-05-31', '60', 'Anna Sant', 4),
(4, 'Malta coastal walk', '2020-06-17', '20', 'Kyle Tanti', 3),
(5, 'Zurrieq to Siggiewi', '2020-06-19', '16', 'Roberta Vassallo', 5),
(6, 'Victoria circular Walk', '2020-06-30', '11', 'Kyle Tanti', 1),
(7, 'Buskett Gardens & Dingli Cliffs', '2020-07-16', '15', 'Robert Pace', 5),
(8, 'The Victoria Lines', '2020-07-21', '30', 'Anna Sant', 2),
(9, 'Valletta to Sliema ', '2020-07-30', '6', 'Kyle Tanti', 2),
(10, 'Ta’ Cenc Cliffs', '2020-07-31', '19', 'Roberta Vassallo', 3),
(11, 'The Tarxien/Hypogeum Temples', '2020-08-01', '110', 'Anna Sant', 5);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
