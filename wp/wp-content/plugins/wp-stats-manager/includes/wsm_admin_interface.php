<?php
if ( ! defined( 'ABSPATH' ) ) exit; 
class wsmAdminInterface{
    private $startWrapper;
    private $endWrapper;
    private $startMetaBox;
    private $endMetaBox;
    private $wsmClear;
    private $objDatabase;
    function __construct(){
        $this->startWrapper='<div class="wrap">';
        $this->endWrapper='</div>';
        $this->startMetaBoxWrapper='<div id="dashboard-widgets-wrap" class="wsmMetaboxContainer"><div id="dashboard-widgets" class="metabox-holder">';
        $this->endMetaBoxWrapper='</div></div>';
        $this->wsmClear='<div class="clear"></div>';
        $this->objDatabase=new wsmDatabase();
		
    }
    
	
	function fnPrintTitle($title){
        return '<h1 class="wsmHead">'.WSM_NAME.'</h1>'.$this->fnPrintHeader($title).'
		<p>
		
			<div id="wsm_subscribe" class="notice notice-info is-dismissible" > 
				<h2 class="alert-heading">'.__("Plugins Market Mailing List", WSM_PREFIX).'</h2><p>
				
				<button type="button" class="btn btn-primary wsm_subscribe_btn" onclick="wsm_open_subscribe_page(\''.get_bloginfo("admin_email").'\')" >'.__("Subscribe", WSM_PREFIX).'</button> '.__("to get latest news and updates, plugin recommendations and configuration help, promotional email with", WSM_PREFIX).' <b style="color:red">'.__("discount codes :)" , WSM_PREFIX).'</b> '.__("or" , WSM_PREFIX).' <a href="#" onclick="wsm_dismiss_notice()">'.__('Dismiss this notice', WSM_PREFIX).'</a></p>
			</div>
			<script>
			function wsm_dismiss_notice()
				{
					localStorage.setItem(\'wsm_subscribed\', \'subsbc_user\');
					document.getElementById("wsm_subscribe").style.display="none";
				}

				function wsm_open_subscribe_page()
				{
					if(localStorage.getItem(\'wsm_subscribed\') !=\'subsbc_user\')
					{
					window.open(\'https://www.plugins-market.com/subscribe-now/?email='.get_bloginfo("admin_email").'\',\'_blank\');
					
					}
				}

				if(localStorage.getItem(\'wsm_subscribed\') ==\'subsbc_user\')
				{
					
					 document.getElementById("wsm_subscribe").style.display="none";
				}
				
				function wsm_upgrade_to_pro()
				{
					
					  jQuery(\'#wsm_modal\').modal();
					
				}
				
			</script>
		</p>';
	
	
			 
			 
    }

	function wsm_upgrade_to_pro()
	{
		return ' style="color:gray" onclick="javascript: wsm_upgrade_to_pro()  "';
	}
	
    function fnPrintHeader($active=""){
        global $wsmRequestArray,$wsmAdminJavaScript;
        
        $current=isset($wsmRequestArray['subPage']) && $wsmRequestArray['subPage']!=''?$wsmRequestArray['subPage']:'';
         
        $header='<div class="wmsHorizontalTab">
                    <ul class="wmsTabList">';
                        $class=$active=='Traffic'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_traffic').'" '.$class.'>'.__('Traffic',WSM_PREFIX).'</a>';
                        $current=$current==''?'Summary':$current;
                        $class=$current=='Summary'?'class="active"':'';
                        $header.='<ul class="wmsSubList sublisthover"><li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_traffic&subPage=Summary').'" '.$class.'>Summary</a></li>';
                        
                        $current=$current==''?'UsersOnline':$current;
                        $class=$current=='UsersOnline'?'class="active"':'';
                        $header .='<li><a '.$this->wsm_upgrade_to_pro().' href="#" '.$class.'>Users Online</a></li>';
                        
                        $current=$current==''?'TrafStats':$current;
                        $class=$current=='TrafStats'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#" '.$class.'>Traffic Stats</a></li></ul>';
                        $header.='</li>';
                        
                        $class=$active=='Traffic Sources'?'class="active"':'';
                        
                        $header.='<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_trafficsrc').'" '.$class.'>'.__('Traffic Sources',WSM_PREFIX).'</a>';
                        
                        $current=$current==''?'RefSites':$current;
                        $class=$current=='RefSites'?'class="active"':'';
                        $header.='<ul class="wmsSubList sublisthover"><li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_trafficsrc&subPage=RefSites').'" '.$class.'>Refering Sites</a></li>';
                        
                        $current=$current==''?'SearchEngines':$current;
                        $class=$current=='SearchEngines'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#"'.$class.'>Search Engines</a></li>';
                        
                        $current=$current==''?'SearchKeywords':$current;
                        $class=$current=='SearchKeywords'?'class="active"':'';
                        $header.='<li><a  '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>Search Keywords</a></li></ul>';
                        
                        $header.='</li>';
							
                        $class=$active=='Visitors'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_visitors').'" '.$class.'>'.__('Visitors',WSM_PREFIX).'</a>';
                        
                        $current=$current==''?'bosl':$current;
                        $class=$current=='bosl'?'class="active"':'';
                        $header.='<ul class="wmsSubList sublisthover"><li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_visitors&subPage=bosl').'" '.$class.'>Browser/OS/Languages</a></li>';
                        
                        $current=$current==''?'GeoLocation':$current;
                        $class=$current=='GeoLocation'?'class="active"':'';
                        $header .='<li><a  '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>GeoLocation</a></li></ul>';
                        $header.='</li>';
                        
                        $class=$active=='Content'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_content').'" '.$class.'>'.__('Content',WSM_PREFIX).'</a>';
                        
                        $current=$current==''?'byURL':$current;
                        $class=$current=='byURL'?'class="active"':'';
                        $header.='<ul class="wmsSubList sublisthover"><li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_content&subPage=byURL').'" '.$class.'>Traffic By URL</a></li>';
                        $current=$current==''?'byTitle':$current;
                        $class=$current=='byTitle'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>Traffic By Title</a></li></ul>';
                        $header.='</li>';
                        
                        //$class=$active=='I.P. Exclusion'?'class="active"':'';
                        //$header.='<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_ipexc').'" '.$class.'>'.__('I.P. Exclusion',WSM_PREFIX).'</a></li>';
                        
						$class=$active=='Settings'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_settings').'" '.$class.'>'.__('Settings',WSM_PREFIX).'</a>';
                        $header.='<ul class="sublist-section wmsSubList sublisthover" data-url="'.admin_url('admin.php?page='.WSM_PREFIX.'_settings').'"><li><a class="" href="#generalsettings">General settings</a></li><li><a class="" href="#ipexclusion">IP Exclusion</a></li><li><a class="" '.$this->wsm_upgrade_to_pro().' href="#" >Email Reports</a></li><li><a class="" '.$this->wsm_upgrade_to_pro().' href="#" >Admin dashboard</a></li><li><a class="" '.$this->wsm_upgrade_to_pro().' href="#" >Plugin Main Page (statistics dashboard)</a></li><li><a class="" '.$this->wsm_upgrade_to_pro().' href="#" >Short-Codes</a></li></ul>';
                        $header.='</li>';
                        
						$class=$active=='Addons'?'class="active"':'';
                        $header.= '<li><a style="background-color:#2196F3; color:white" href="'.admin_url('admin.php?page='.WSM_PREFIX.'_addons').'" '.$class.'>'.__('Add ons',WSM_PREFIX).'</a></li>';
                       
						$header.='<li><a href="http://plugins-market.com/product/visitor-statistics-pro/#upgrade" '.$class.' target="_blank" style="background-color:green; color:white">'.__('Upgrade to Pro',WSM_PREFIX).'</a>';
                        $header.='</li>';
						
                        
                    $header.='</ul>'.$this->wsmClear;
                   
                    if($active=='Traffic'){
                        $header.='<ul class="wmsSubList">';
                        $current=$current==''?'Summary':$current;
                        $class=$current=='Summary'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_traffic').'&subPage=Summary" '.$class.'>'.__('Summary',WSM_PREFIX).'</a></li>';
                        $class=$current=='UsersOnline'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>'.__('Users Online',WSM_PREFIX).'</a></li>';
                        $class=$current=='TrafStats'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#" '.$class.'>'.__('Traffic Stats',WSM_PREFIX).'</a></li>';
                        $header.='</ul>'.$this->wsmClear;
                        if($current=='UsersOnline'){
                            $wsmAdminJavaScript.='arrLiveStats.push("wsmTopTitle");';
                            $onlineVisitors=$this->objDatabase->fnGetTotalVisitorsCount('Online');
                            $browsingPages=$this->objDatabase->fnGetTotalBrowsingPages();
                            $subTab=isset($wsmRequestArray['subTab']) && $wsmRequestArray['subTab']!=''?$wsmRequestArray['subTab']:'';
                           
                            $header.= '<div class="wsmTopTitle"><span class="wsmOnline">'.__('Users Online',WSM_PREFIX).'&nbsp;:&nbsp;<b>'.$onlineVisitors.'</b></span><span class="wsmBrowsing">'.__('Browing',WSM_PREFIX).':&nbsp;<b>'.$browsingPages.'</b>&nbsp;'.__('pages',WSM_PREFIX).'</span></div>';
                            $subClass=$subTab=='summary'?'class="active"':'';
                            $header.= '<ul class="wmsTabList wsmSubTabList">
                            <li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=summary" '.$subClass.'>'.__('Summary',WSM_PREFIX).'</a></li>';
                            $subClass=$subTab=='recent'?'class="active"':'';
                            $header.= '<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=recent" '.$subClass.'>'.__('Recent',WSM_PREFIX).'</a></li>';
                            $subClass=$subTab=='mavis'?'class="active"':'';
                            $header.= '<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=mavis" '.$subClass.'>'.__('Most Active Visitors',WSM_PREFIX).'</a></li>';
                            $subClass=$subTab=='popPages'?'class="active"':'';
                            $header.= '<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=popPages" '.$subClass.'>'.__('Popular Pages',WSM_PREFIX).'</a></li>';
                            $subClass=$subTab=='popReferrer'?'class="active"':'';
                            $header.= '<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_traffic').'&subPage=UsersOnline&subTab=popReferrer" '.$subClass.'>'.__('Popular Referrers',WSM_PREFIX).'</a></li>';
                            $subClass=$subTab=='geoLocation'?'class="active"':'';
                            $header.= '<li><a  '.$this->wsm_upgrade_to_pro().' href="#" '.$subClass.'>'.__('Geo Location',WSM_PREFIX).'</a></li>';
                            $header.= '</ul>'.$this->wsmClear;
                        }
                    }
                    if($active=='Traffic Sources'){
                        $header.='<ul class="wmsSubList">';
                        $current=$current==''?'RefSites':$current;
                        $class=$current=='RefSites'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_trafficsrc').'&subPage=RefSites" '.$class.'>'.__('Refering Sites',WSM_PREFIX).'</a></li>';
                        $class=$current=='SearchEngines'?'class="active"':'';
                        $header.='<li><a  '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>'.__('Search Engines',WSM_PREFIX).'</a></li>';
                        $class=$current=='SearchKeywords'?'class="active"':'';
                        $header.='<li><a  '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>'.__('Search Keywords',WSM_PREFIX).'</a></li>';
                        $header.='</ul>';
                    }
                    if($active=='Visitors'){
                        $header.='<ul class="wmsSubList">';
                        $current=$current==''?'bosl':$current;
                        $class=$current=='bosl'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_visitors').'&subPage=bosl" '.$class.'>'.__('Browser/OS/Languages',WSM_PREFIX).'</a></li>';
                        $class=$current=='GeoLocation'?'class="active"':'';
                        $header.='<li><a  '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>'.__('GeoLocation',WSM_PREFIX).'</a></li>';
                        $header.='</ul>';
                    }
                    if($active=='Content'){
                        $header.='<ul class="wmsSubList">';
                        $current=$current==''?'byURL':$current;
                        $class=$current=='byURL'?'class="active"':'';
                        $header.='<li><a href="'.admin_url('admin.php?page='.WSM_PREFIX.'_content').'&subPage=byURL" '.$class.'>'.__('Traffic By URL',WSM_PREFIX).'</a></li>';
                        $class=$current=='byTitle'?'class="active"':'';
                        $header.='<li><a '.$this->wsm_upgrade_to_pro().' href="#"  '.$class.'>'.__('Traffic By Title',WSM_PREFIX).'</a></li>';
                        $header.='</ul>';
                    }
                  $header.='</div>'.$this->wsmClear;
        return $header;
                  //$color = get_user_meta(get_current_user_id(), 'admin_color', true);
    }
    function fnShowTodayStats(){
        echo  do_shortcode("[".WSM_PREFIX."_showDayStats]");
        echo  do_shortcode("[".WSM_PREFIX."_showCurrentStats]");
        echo  do_shortcode("[".WSM_PREFIX."_showForeCast]");
        //echo $html;
    }
    
    function fnShowDailyStatBox($post, $arrParam){
        echo do_shortcode("[".WSM_PREFIX."_showDayStatBox]");       
    }
    function fnStatFilterBox(){
        echo  do_shortcode("[".WSM_PREFIX."_showStatFilterBox]"); 
    }
    function fnStatFilterBox4Referral(){
        echo  do_shortcode("[".WSM_PREFIX."_showStatFilterBox hide='Monthly' source='Referral']"); 
    }
    function fnShowTopReferrerSites(){
        echo  do_shortcode("[".WSM_PREFIX."_showTopReferrerList searchengine='".( isset($_REQUEST['subPage']) && $_REQUEST['subPage'] == 'SearchEngines' ? 1 : '' )."' ]"); 
    }
	function fnStatsSearchKeywords(){
		echo do_shortcode("[".WSM_PREFIX."_showStatKeywords]");
	}
	function fnShowOsStatBox(){
		echo do_shortcode("[".WSM_PREFIX."_showVisitorsDetailGraph]");
	}
	function fnShowVisitorsDetails(){
		echo do_shortcode("[".WSM_PREFIX."_showVisitorsDetail]");
	}
	function fnShowGeoLocationStats(){
		echo do_shortcode("[".WSM_PREFIX."_showGeoLocationGraph]");
	}
	function fnShowGeoLocationDetails(){
		echo do_shortcode("[".WSM_PREFIX."_showGeoLocationDetails]");
	}
	
    function fnShowDaysStatsGraph(){
        $arrPostData=wsmSanitizeFilteredPostData();
        if($arrPostData['filterWay']!='Range'){
            echo  do_shortcode("[".WSM_PREFIX."_showDayStatsGraph]");
        }
        echo  do_shortcode("[".WSM_PREFIX."_showTrafficStatsList]");
    }
    function fnShowGenStats(){
        echo  do_shortcode("[".WSM_PREFIX."_showGenStats]");
        //echo $html;
    }
    function fnShowLastDaysStats(){
        echo  do_shortcode("[".WSM_PREFIX."_showLastDaysStats]");
        echo  do_shortcode("[".WSM_PREFIX."_showLastDaysStatsChart id='LastDaysChart2']");
        //echo $html;
    }
    function fnShowGeoLocationChart(){
        echo  do_shortcode("[".WSM_PREFIX."_showGeoLocation]");
    }
    function fnShowRecentVisitedPages(){
        echo  do_shortcode("[".WSM_PREFIX."_showRecentVisitedPages]");
    }
    function fnShowRecentVisitedPagesDetails(){
        echo  do_shortcode("[".WSM_PREFIX."_showRecentVisitedPagesDetails]");
    }
    function fnShowPopularPages(){
        echo  do_shortcode("[".WSM_PREFIX."_showPopularPages]");
    }
    function fnShowPopularReferrers(){
        echo  do_shortcode("[".WSM_PREFIX."_showPopularReferrers]");
    }
    function fnShowMostActiveVisitors(){
        echo  do_shortcode("[".WSM_PREFIX."_showMostActiveVisitors]");
    }
    function fnShowMostActiveVisitorsDetails(){
        echo  do_shortcode("[".WSM_PREFIX."_showMostActiveVisitorsDetails]");
    }
    function fnShowMostActiveVisitorsGeo(){
        echo  do_shortcode("[".WSM_PREFIX."_showMostActiveVisitorsGeo]");
    }
    function fnShowMostActiveVisitorsGeoDetails(){
        echo  do_shortcode("[".WSM_PREFIX."_showMostActiveVisitorsGeo height='450px' zoom='2']");       
    }
    function fnShowActiveVistiorsCountByCountry(){
         echo  do_shortcode("[".WSM_PREFIX."_showActiveVisitorsByCountry]");
    }
    function fnShowActiveVistiorsCountByCity(){
         echo  do_shortcode("[".WSM_PREFIX."_showActiveVisitorsByCity]");
    }
    function fnShowReffererStatBox(){
         echo  do_shortcode("[".WSM_PREFIX."_showRefferStatsBox searchengine='".( isset($_REQUEST['subPage']) && $_REQUEST['subPage'] == 'SearchEngines' ? 1 : '' )."' ]");
    }
    function fnShowReffererSearchEngineStatBox(){
         echo  do_shortcode("[".WSM_PREFIX."_showRefferStatsBox searchengine='1']");
    }
	function fnShowSearchEngineSummary(){
		echo do_shortcode('['.WSM_PREFIX.'_showSearchEngineSummary]');
	}
	function fnShowContentByURL(){
		echo do_shortcode("[".WSM_PREFIX."_showContentByURL]");
	}
	function fnShowContentURLStats(){
		echo do_shortcode("[".WSM_PREFIX."_showContentByURLStats]");
	}
	function fnIPExclusion(){
		echo do_shortcode("[".WSM_PREFIX."_showIPExclustion]");
	}
	function fnShowTitleCloud(){
		echo do_shortcode("[".WSM_PREFIX."_showTitleCloud]");
	}
    function wsmSavePluginSettings($arrPostData){
        $tzstring = get_option('wsmTimezoneString');
       
	   if(null !== (sanitize_text_field($_POST[WSM_PREFIX.'TimezoneString'])) && sanitize_text_field($_POST[WSM_PREFIX.'TimezoneString'])!=''){
            //if($tzstring!==$_POST[WSM_PREFIX.'TimezoneString']){
                update_option(WSM_PREFIX.'TimezoneString',sanitize_text_field($_POST[WSM_PREFIX.'TimezoneString']));
                wsmInitPlugin::wsm_fnCreateImportantViews();
                wsmInitPlugin::wsm_createMonthWiseViews();
				//}
        }
        if(null !== (sanitize_text_field($_POST[WSM_PREFIX.'ChartDays'])) && sanitize_text_field($_POST[WSM_PREFIX.'ChartDays'])!=''){
            update_option(WSM_PREFIX.'ChartDays',sanitize_text_field($_POST[WSM_PREFIX.'ChartDays']));
        }
        if(null !== (sanitize_text_field($_POST[WSM_PREFIX.'Country'])) && sanitize_text_field($_POST[WSM_PREFIX.'Country']) !=''){
            update_option(WSM_PREFIX.'Country', sanitize_text_field($_POST[WSM_PREFIX.'Country']));
        }
        if(null !== (sanitize_text_field($_POST[WSM_PREFIX.'GoogleMapAPI'])) && sanitize_text_field($_POST[WSM_PREFIX.'GoogleMapAPI'])!=''){
            update_option(WSM_PREFIX.'GoogleMapAPI',sanitize_text_field($_POST[WSM_PREFIX.'GoogleMapAPI']));
        }
        if(null !== (sanitize_text_field($_POST[WSM_PREFIX.'ArchiveDays'])) && sanitize_text_field($_POST[WSM_PREFIX.'ArchiveDays'])!=''){
            update_option(WSM_PREFIX.'ArchiveDays', sanitize_text_field($_POST[WSM_PREFIX.'ArchiveDays']));
        }
        if(null !== (sanitize_text_field($_POST[WSM_PREFIX.'KeepData'])) && sanitize_text_field($_POST[WSM_PREFIX.'KeepData'])!=''){
            update_option(WSM_PREFIX.'KeepData',"1");
        }
        else
			update_option(WSM_PREFIX.'KeepData',"0");
			 
		update_option(WSM_PREFIX.'ReportScheduleTime',sanitize_text_field($_POST[WSM_PREFIX.'ReportScheduleTime']));
		update_option(WSM_PREFIX.'ReportStats',sanitize_text_field($_POST[WSM_PREFIX.'ReportStats']));
		update_option(WSM_PREFIX.'ReportEmails',sanitize_text_field($_POST[WSM_PREFIX.'ReportEmails']));
		update_option(WSM_PREFIX.'SiteDashboardNormalWidgets',sanitize_text_field($_POST[WSM_PREFIX.'SiteDashboardNormalWidgets']));
		update_option(WSM_PREFIX.'SiteDashboardSideWidgets',sanitize_text_field($_POST[WSM_PREFIX.'SiteDashboardSideWidgets']));
		update_option(WSM_PREFIX.'Dashboard_widget',sanitize_text_field($_POST[WSM_PREFIX.'Dashboard_widget']));
		update_option(WSM_PREFIX.'SitePluginNormalWidgets',sanitize_text_field($_POST[WSM_PREFIX.'SitePluginNormalWidgets']));
		update_option(WSM_PREFIX.'SitePluginSideWidgets',sanitize_text_field($_POST[WSM_PREFIX.'SitePluginSideWidgets']));
		update_option(WSM_PREFIX.'Plugin_widget',sanitize_text_field($_POST[WSM_PREFIX.'Plugin_widget']));
    }
	function wsmViewAddOns(){
		$addons = apply_filters( 'wsm_addons', array() );
		$addons_settings = apply_filters( 'wsm_addons_settings', array() );
		if( null !== (sanitize_text_field($_POST['action']) ) && sanitize_text_field($_POST['action']) == 'save_wsm_addons' ){
			echo sprintf( '<div class="notice updated"><p class="message">%s</p></div>', __( 'All setting is saved.', 'wsm' ) );
		}
		echo $this->fnPrintTitle('Addons');
		echo '<div class="wsm_addons_panel">';
		if( is_array( $addons ) && count( $addons ) ){
			echo '<form method="post">';
			echo '<ul class="li-section">';
			$class = 'active';
			$visible = 'table';
			$active_tab = '';
			if( null !== (sanitize_text_field($_POST['tab-li-active']) ) && !empty( sanitize_text_field($_POST['tab-li-active']) ) ){
				$active_tab = sanitize_text_field($_POST['tab-li-active']);
				$visible = $class = '';
			}
			foreach( $addons as $key => $addon ){
				if( $active_tab == '#'.$key ){
					$class = 'active';
				}
				echo sprintf('<li><a class="%s" href="#%s">%s</a></li>', $class, $key, $addon );
				$class = '';
			}
			echo '</ul>';
			echo '<div class="li-section-table">';
			foreach( $addons_settings as $key => $addons_setting ){
				$setting_values = get_option( $key. '_settings' ); 
				if( $active_tab == '#'.$key ){
					$visible = 'table';
				}
				echo sprintf( '<table id="%s" style="display:%s" class="form-table">', $key, $visible );
				$visible = '';
				$field_name = $key.'_enable';
				$checked = isset( $setting_values[$field_name] ) && $setting_values[$field_name] ? 'checked' : ''; 
				echo sprintf( '<tr><th>%s</th><td><label class="switch"><input name="%s" type="checkbox" %s value="1"><div class="slider round"></div></label></td></tr>', __( 'Enable', 'wsm' ), $field_name, $checked );
				foreach( $addons_setting as $setting ){
					$field_name = $setting['id'];
					echo sprintf( '<tr><th>%s</th><td>', $setting['label'] );
					switch( $setting['type'] ){
						case 'checkbox';
							$checked = isset( $setting_values[$field_name] ) && $setting_values[$field_name] ? 'checked' : ''; 
							echo sprintf('<label class="switch"><input name="%s" type="checkbox" %s value="1"><div class="slider round"></div></label>', $setting['id'], $checked );
							break;
						case 'select':
							echo sprintf( '<select name="%s">', $setting['id'] );
							$selected_value = isset( $setting['default'] ) ?  $setting['default'] : '';
							$selected_value = isset( $setting_values[$field_name] ) && $setting_values[$field_name] ? $setting_values[$field_name] : $selected_value; 
							if( isset( $setting['values'] ) ){
								foreach( $setting['values'] as $key => $value ){
									$selected = '';
									if( $selected_value == $key ){
										$selected = 'selected';
									}
									echo sprintf( '<option %s value="%s">%s</option>', $selected, $key, $value );	
								}		
							}
							echo '</select>';
							break;	
						case 'post_type':
							echo sprintf( '<select multiple="multilple" class="posttype_dropdown" name="%s[]">', $setting['id'] );
							$selected_value = isset( $setting['default'] ) ?  $setting['default'] : array();
							$selected_value = isset( $setting_values[$field_name] ) && $setting_values[$field_name] ? $setting_values[$field_name] : $selected_value; 
							$args = array(
							   'public'   => true
							);
							$post_types = get_post_types($args, 'names');
								foreach( $post_types as $value ){
									if( $value != 'attachment' ){
										$selected = '';
										if( in_array( $value, $selected_value ) ){
											$selected = 'selected';
										}
										echo sprintf( '<option %s value="%s">%s</option>', $selected, $value, $value );	
									}
								}		
							echo '</select>';
							break;
					}
					echo '</td></tr>';
					
				}
				echo '</table>';
			}
			echo '</div>';
			echo '<input type="hidden" name="tab-li-active" id="tab-li-active" value="">';
			echo '<input type="hidden" name="action" value="save_wsm_addons" />';
			echo sprintf( '<p class="submit"><input type="submit" class="button button-primary" value="%s"></p>', __('Save changes', 'wsm') );
			echo '</form>';
		}else{
			echo __( '<p><br /><i>There is no add ons intalled yet.</i></p><p><table border="0" cellpadding="20px"><tr><td><a href="http://plugins-market.com/product/post-stats-add-on/" target="_blank"><img src="http://plugins-market.com/wp-content/uploads/2018/04/poststats-addon.png" width="150px"></a></td><td><a href="http://plugins-market.com/product/visitor-statistics-mini-chart-add-on/" target="_blank"><img src="http://plugins-market.com/wp-content/uploads/2018/04/minichart-addon.png" width="150px"></a></td></tr></table></p>', 'wsm' );
		}
		echo '</div>';
	}
    function wsmViewSettings(){
        global $wsmAdminJavaScript,$wsmAdminPageHooks;
        if(null !== (sanitize_text_field($_POST[WSM_PREFIX.'_form'])) && sanitize_text_field($_POST[WSM_PREFIX.'_form'])==WSM_PREFIX.'_frmSettings'){
            $this->wsmSavePluginSettings($_POST);
        }
        $html=$this->startWrapper;
        $html.=$this->fnPrintTitle('Settings');
        $current_offset = get_option('gmt_offset');
        $tzstring = get_option(WSM_PREFIX.'TimezoneString');
        $chartDays = get_option(WSM_PREFIX.'ChartDays');
        $country = get_option(WSM_PREFIX.'Country');
        $googleMapAPI = get_option(WSM_PREFIX.'GoogleMapAPI');
        $ArchiveDays = get_option(WSM_PREFIX.'ArchiveDays');
        $KeepData = get_option(WSM_PREFIX.'KeepData');
        $KeepDataChk="";
        if($KeepData=="1")
			$KeepDataChk = "checked='checked'";
		
        $reportScheduleTime = get_option(WSM_PREFIX.'ReportScheduleTime');
        $reportStats = get_option(WSM_PREFIX.'ReportStats');
        $reportEmails = get_option(WSM_PREFIX.'ReportEmails');
		$siteDashboardNormalWidgets = get_option(WSM_PREFIX.'SiteDashboardNormalWidgets');
		$siteDashboardSideWidgets = get_option(WSM_PREFIX.'SiteDashboardSideWidgets');
		$dashboard_widget = get_option(WSM_PREFIX.'Dashboard_widget');
		$sitePluginNormalWidgets = get_option(WSM_PREFIX.'SitePluginNormalWidgets');
		$sitePluginSideWidgets = get_option(WSM_PREFIX.'SitePluginSideWidgets');
		$plugin_widget = get_option(WSM_PREFIX.'Plugin_widget');
		
        $check_zone_info = true;
        // Remove old Etc mappings. Fallback to gmt_offset.
        if ( false !== strpos($tzstring,'Etc/GMT') )
            $tzstring = '';
        if ( empty($tzstring) ) { // Create a UTC+- zone if no timezone string exists
            $check_zone_info = false;
            if ( 0 == $current_offset )
                $tzstring = 'UTC+0';
            elseif ($current_offset < 0)
                $tzstring = 'UTC' . $current_offset;
            else
                $tzstring = 'UTC+' . $current_offset;
        }
		$mailTiming = '<select id="'.WSM_PREFIX.'ReportScheduleTime" name="'.WSM_PREFIX.'ReportScheduleTime"><option value=""></option>';
		$scheduleArray = array( 1 => 'Every Day', 3 => 'Every 3 Days', 7 => 'Every Week', 30 => 'Every Month' );
		foreach( $scheduleArray as $key => $value ){
			$mailTiming .= '<option '.($reportScheduleTime == $key ? 'selected' : '').' value="'.$key.'">'.$value.'</option>';
		}
		$mailTiming .= '</select>';
		
		$report_stats_list = array('general_stats_new' => 'General Stats',
									'daily_stats'	=>	'Daily Stats',
									'referral_website_stats'	=>	'Referral Website Stats',
									'search_engine_stats'	=>	'Top Search Engine Stats',
									'traffic_by_title_stats' => 'Title Stats',
									'top_search_engine_stats' => 'Top Search Engine Stats',
									'os_wise_visitor_stats'	=>	'OS base Visitor Stats',
									'browser_wise_visitor_stats'	=>	'Browser base Visitor Stats',
									'screen_wise_visitor_stats'	=>	'Screen base Visitor Stats',
									'country_wise_visitor_stats'	=>	'Today Countries Stats',
									'city_wise_visitor_stats'	=>	'Today Cities Stats',
									/*'recent_visit_pages' => 'Traffic By Title',*/
									'recent_active_visitors' => 'Users Online'
							);
		
		//$reportStatsHTML = '<select id="'.WSM_PREFIX.'ReportStats" multiple name="'.WSM_PREFIX.'ReportStats[]">';
		$reportStatsHTML = '<table class="report_list_table">';
		$reportWidget = $dashboardNormalWidget = $dashboardSideWidget = $pluginNormalWidget = $pluginSideWidget = '';
		
		$dashboardNormalWidgetList = $siteDashboardNormalWidgets ? explode(',', $siteDashboardNormalWidgets) : array();
		$dashboardSideWidgetList = $siteDashboardSideWidgets ? explode(',', $siteDashboardSideWidgets) : array();
		
		$pluginNormalWidgetList = $sitePluginNormalWidgets ? explode(',', $sitePluginNormalWidgets) : array();
		$pluginSideWidgetList = $sitePluginSideWidgets ? explode(',', $sitePluginSideWidgets) : array();
		
		foreach( $report_stats_list as $key => $value ){
			//$reportStatsHTML .= '<option '.( (is_array($reportStats) && in_array( $key, $reportStats)) ? 'selected' : '' ).' value="'.$key.'">'.$value.'</option>';
			$status =(is_array($reportStats) && in_array( $key, $reportStats)) ? 1 : 0;
			if( !in_array($key, array( 'recent_visit_pages', 'traffic_by_title_stats', 'recent_active_visitors' ) ) ){
				$reportStatsHTML .= sprintf( '<tr><td>%s</td><td><label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></td></tr>', $value, WSM_PREFIX.'ReportStats[]', ($status?'checked':''), $key );
			}
			$reportWidget .= sprintf( '<div data-id="%s">%s</div>', $key, $value );
			$dashboardNormalWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $value, WSM_PREFIX.'Dashboard_widget[normal][]', ($status?'checked':''), $key );
			$pluginNormalWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $value, WSM_PREFIX.'Plugin_widget[normal][]', ($status?'checked':''), $key );
		}
		if( count( $dashboardNormalWidgetList ) ){
			$dashboardNormalWidget = '';
			foreach( $dashboardNormalWidgetList as $widget ){
				$status = 0;
				$widget = trim($widget);
				if( is_array( $dashboard_widget ) && isset( $dashboard_widget['normal'] ) && in_array( $widget, $dashboard_widget['normal'] ) ){
					$status = 1;
				}
				$dashboardNormalWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $report_stats_list[$widget], WSM_PREFIX.'Dashboard_widget[normal][]', ($status?'checked':''), $widget );
			}
		}
		if( count( $dashboardSideWidgetList ) ){
			foreach( $dashboardSideWidgetList as $widget ){
				$status = 0;
				$widget = trim($widget);
				if( is_array( $dashboard_widget ) && isset( $dashboard_widget['side'] ) && in_array( $widget, $dashboard_widget['side'] ) ){
					$status = 1;
				}
				$dashboardSideWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $report_stats_list[$widget], WSM_PREFIX.'Dashboard_widget[side][]', ($status?'checked':''), $widget );
			}
		}
		if( count( $pluginNormalWidgetList ) ){
			$pluginNormalWidget = '';
			foreach( $pluginNormalWidgetList as $widget ){
				$status = 0;
				$widget = trim($widget);
				if( is_array( $plugin_widget ) && isset( $plugin_widget['normal'] ) && in_array( $widget, $plugin_widget['normal'] ) ){
					$status = 1;
				}
				$pluginNormalWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $report_stats_list[$widget], WSM_PREFIX.'Plugin_widget[normal][]', ($status?'checked':''), $widget );
			}
		}
		if( count( $pluginSideWidgetList ) ){
			foreach( $pluginSideWidgetList as $widget ){
				$status = 0;
				$widget = trim($widget);
				if( is_array( $plugin_widget ) && isset( $plugin_widget['side'] ) && in_array( $widget, $plugin_widget['side'] ) ){
					$status = 1;
				}
				$pluginSideWidget .= sprintf( '<li><i class="handle"></i> %s <label class="switch"><input name="%s" type="checkbox" %s value="%s" ><div class="slider round"></div></label></li>', $report_stats_list[$widget], WSM_PREFIX.'Plugin_widget[side][]', ($status?'checked':''), $widget );
			}
		}
		//$reportStatsHTML .= '</select>';
		$reportStatsHTML .= '</table>';
		$wsmStatistics=new wsmStatistics;
		//print_r($_POST);
		//echo 'active'.$_POST['tab-li-active'];
        $html.='<form name="'.WSM_PREFIX.'_frmSettings" method="post">';
        $html.='<input type="hidden" name="'.WSM_PREFIX.'_form" value="'.WSM_PREFIX.'_frmSettings">';
        $generalsettings="active";
        $ipexclusion=$sitedashboard=$report=$summarywidget=$shortcodelist='';
		$ipexclusion1=$generalsettings1=$sitedashboard1=$report1=$summarywidget1=$shortcodelist1='';
        if(null !== (sanitize_text_field($_POST['tab-li-active'])) && sanitize_text_field($_POST['tab-li-active'])!=='')
        {
			if(sanitize_text_field($_POST['tab-li-active'])=="#generalsettings")
			{
					$generalsettings="active";
					$ipexclusion=$sitedashboard=$report=$summarywidget=$shortcodelist='';
					
					$generalsettings1='style="display: table;"';
					$ipexclusion1=$sitedashboard1=$report1=$summarywidget1=$shortcodelist1='style="display: none;"';
			}
			else if(sanitize_text_field($_POST['tab-li-active'])=="#ipexclusion")
			{
					$ipexclusion="active";
					$generalsettings=$report=$summarywidget=$shortcodelist='';
					
					$ipexclusion1='style="display: table;"';
					$generalsettings1=$report1=$summarywidget1=$shortcodelist1='style="display: none;"';
			}			
			else if(sanitize_text_field($_POST['tab-li-active'])=="#sitedashboard")
			{
					$sitedashboard="active";
					$ipexclusion=$generalsettings=$report=$summarywidget=$shortcodelist='';
					
					$sitedashboard1='style="display: table;"';
					$ipexclusion1=$generalsettings1=$report1=$summarywidget1=$shortcodelist1='style="display: none;"';
			}
			else if(sanitize_text_field($_POST['tab-li-active'])=="#report")
			{
					$report="active";
					$ipexclusion=$generalsettings=$sitedashboard=$summarywidget=$shortcodelist='';
					
					$report1='style="display: table;"';
					$ipexclusion1=$generalsettings1=$sitedashboard1=$summarywidget1=$shortcodelist1='style="display: none;"';
			}
			else if(sanitize_text_field($_POST['tab-li-active'])=="#summarywidget")
			{
					$summarywidget="active";
					$ipexclusion=$generalsettings=$sitedashboard=$report=$shortcodelist='';
					
					$summarywidget1='style="display: table;"';
					$ipexclusion1=$generalsettings1=$sitedashboard1=$report1=$shortcodelist1='style="display: none;"';
			}
			else if(sanitize_text_field($_POST['tab-li-active'])=="#shortcodelist")
			{
					$shortcodelist="active";
					$ipexclusion=$generalsettings=$sitedashboard=$report=$summarywidget='';
					
					$shortcodelist1='style="display: table;"';
					$ipexclusion1=$generalsettings1=$sitedashboard1=$report1=$summarywidget1='style="display: none;"';
			}
			
		}
		ob_start(); 
		include WSM_DIR."includes/wsm_shortcodeTable.php"; 
		$shortCodeData=ob_get_contents(); 
		ob_clean();
        $html.='<ul class="li-section">
					<li><a class="'.$generalsettings.'" href="#generalsettings">General settings</a></li>
					<li><a class="'.$ipexclusion.'" href="#ipexclusion">IP Exclusion</a></li>
					<li><a class="'.$report.'" href="#" '.$this->wsm_upgrade_to_pro().' >Email Reports</a></li>
					<li><a class="'.$sitedashboard.'" href="#" '.$this->wsm_upgrade_to_pro().'>Admin dashboard</a></li>
					<li><a class="'.$summarywidget.'" href="#" '.$this->wsm_upgrade_to_pro().'>Plugin Main Page (statistics dashboard)</a></li>
					<li><a class="'.$shortcodelist.'" href="#" '.$this->wsm_upgrade_to_pro().'>Short-Codes</a></li>
				</ul>';
        $html.='<div class="li-section-table"><table class="form-table" id="generalsettings" '.$generalsettings1.'><tbody>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'TimezoneString">'.__('Timezone',WSM_PREFIX).'</label></th>
                    <td>'.$this->wsmGetCountryDropDown($country).$this->wsmGetTimeZoneDropDown($tzstring).'
                    <p class="description" id="timezone-description">'.__( 'Choose either a city in the same timezone as you or a UTC timezone offset.',WSM_PREFIX ).'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'ChartDays">'.__('Chart Days',WSM_PREFIX).'</label></th>
                    <td>'.$this->wsmGetChartDaysDropDown($chartDays).'
                    <p class="description">'.__( 'Choose number of days to show statistics on the summary page.',WSM_PREFIX ).'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'GoogleMapAPI">'.__('Google Map API',WSM_PREFIX).'</label></th>
                    <td><input type="text" id="'.WSM_PREFIX.'GoogleMapAPI" name="'.WSM_PREFIX.'GoogleMapAPI" value="'.$googleMapAPI.'"/>
                    <p class="description">'.__( 'Enter Google Map API key. You can find how to get the key from this url - <a href="https://developers.google.com/maps/documentation/javascript/get-api-key">https://developers.google.com/maps/documentation/javascript/get-api-key</a>. This key is used to verify from google map.',WSM_PREFIX ).'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'ArchiveDays">'.__('Archive Data',WSM_PREFIX).'</label></th>
                    <td>'.$this->wsmGetArchiteDaysDropDown($ArchiveDays).'
						<p class="description">'.__('You can set archive data setting for 30 days or 60 days.').'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'KeepData1">'.__('Keep Data',WSM_PREFIX).'</label></th>
                    <td>
						<input type="checkbox" name="'.WSM_PREFIX.'KeepData" id="'.WSM_PREFIX.'KeepData" value="1" '.$KeepDataChk.'/> <label for="'.WSM_PREFIX.'KeepData">Keep data after unistalling the plugin?</label>
						<p class="description">'.__('Check to keep data and uncheck to remove data.').'</p>
                    </td>
                </tr>
                </tbody></table>
                <table class="form-table" id="report" '.$report1.'><tbody>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'ReportScheduleTime">'.__('Scheduled Report Time',WSM_PREFIX).'</label></th>
                    <td>'.$mailTiming.'
                    <p class="description">'.__( 'Select time for receiving report mail.',WSM_PREFIX ).'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'ReportStats">'.__('Reports',WSM_PREFIX).'</label></th>
                    <td>'.$reportStatsHTML.'
                    <p class="description">'.__( 'Select stats type which you want to receive as report in mail.',WSM_PREFIX ).'</p>
                    </td>
                </tr>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'ReportEmails">'.__('Report Notification',WSM_PREFIX).'</label></th>
                    <td><textarea cols="50" id="'.WSM_PREFIX.'ReportEmails" name="'.WSM_PREFIX.'ReportEmails">'.$reportEmails.'</textarea>&nbsp;&nbsp;<a href="#" class="button button-primary send_test_mail">Send Test Mail</a>
                    <p class="description">'.__( 'Add more than one email by comma seperator.',WSM_PREFIX ).'</p>
                    </td>
                </tr></tbody></table>
                <table class="form-table" id="sitedashboard" '.$sitedashboard1.'><tbody>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'SiteDashboardWidgets">'.__('Site Dashboard Widgets',WSM_PREFIX).'</label></th>
                    <td>
					<div class="site_dashboard_widget_panel"></div>
					<table><tr><th>Normal</th><th>Side</th></tr><tr><td><ul class="site_dashboard_widget_handler" id="site_dashboard_widget_handler_1">'.$dashboardNormalWidget.'</ul></td><td><ul id="site_dashboard_widget_handler_2" class="site_dashboard_widget_handler">'.$dashboardSideWidget.'</ul></td></tr></table>
                    <p class="description">'.__( 'You can drag and drop widget here.',WSM_PREFIX ).'</p>
					<input type="hidden" name="'.WSM_PREFIX.'SiteDashboardNormalWidgets"  value="" />
					<input type="hidden" name="'.WSM_PREFIX.'SiteDashboardSideWidgets" value="" />
                    </td>
                </tr></tbody></table>
                <table class="form-table" id="summarywidget" '.$summarywidget1.'><tbody>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'SitePluginWidgets">'.__('Plugin Summary Widgets',WSM_PREFIX).'</label></th>
                    <td>
					<div class="site_dashboard_widget_panel"></div>
					<table><tr><th>Normal</th><th>Side</th></tr><tr><td><ul class="site_plugin_widget_handler" id="site_plugin_widget_handler_1">'.$pluginNormalWidget.'</ul></td><td><ul id="site_plugin_widget_handler_2" class="site_plugin_widget_handler">'.$pluginSideWidget.'</ul></td></tr></table>
                    <p class="description">'.__( 'You can drag and drop widget here.',WSM_PREFIX ).'</p>
					<input type="hidden" name="'.WSM_PREFIX.'SitePluginNormalWidgets"  value="" />
					<input type="hidden" name="'.WSM_PREFIX.'SitePluginSideWidgets" value="" />
                    </td>
                </tr>
                </tbody></table>
                 <table class="form-table myshortcodelist" id="shortcodelist" '.$shortcodelist1.'><tbody>
                <tr>
                    <th scope="row"><label for="'.WSM_PREFIX.'">'.__('Shortcodes',WSM_PREFIX).'</label></th>
                </tr>
                <tr>
					<td>
					<div class="shortcode_panel">';
						$html.=$shortCodeData;
			$html.='</div>
                    <p class="description">'.__( 'Shortcode lists are going to display here.',WSM_PREFIX ).'</p>
					</td>
                </tr>
                
                </tbody></table>
                </div>
                
                <p class="submit"><input type="hidden" name="tab-li-active" id="tab-li-active" value=""><input type="submit" name="submit" id="submit" class="button button-primary" value="Save Changes"></p>';
        $html.='</form>';
        
        $html .= '
                <table class="form-table" style="display:none" id="ipexclusion" '.$ipexclusion1.'><tbody>
                <tr>
					
                    <td>
						<div id="dashboard-widgets-wrap" class="wsmMetaboxContainer">
							<div id="dashboard-widgets" class="metabox-holder">
								<form name="wsmmainMetboxForm" id="wsmmainMetboxForm" method="post">
									<input id="_wpnonce" name="_wpnonce" value="100250cbd1" type="hidden"><input type="hidden" name="_wp_http_referer" value="/wp-admin/admin.php?page=wsm_ipexc" />
									<div id="wsm-postbox-container-1" class="postbox-containe">
										<div id="bottom-sortables" class="meta-box-sortables ui-sortable">
											<div id="wsm_ipexc" class="postbox">
													<h2 class="hndle ui-sortable-handle test"><span>I.P. Exclution</span></h2>					 
													<div class="inside">'.$wsmStatistics->wsm_showIPExclustion('').'</div>
											</div>
										</div>
									</div>
									<input type="hidden" id="meta-box-order-nonce" name="meta-box-order-nonce" value="9c03ae7329" /><input type="hidden" id="closedpostboxesnonce" name="closedpostboxesnonce" value="4f4cab2048" />
								</form>
							</div>
						</div>
					</td>
                </tr>
               </tbody></table>';
        $wsmAdminJavaScript.='
            jQuery("#'.WSM_PREFIX.'Country").on(\'change\', function() {
				$(\'#submit\').prop(\'disabled\', true);
              jQuery.ajax({
                   type: "POST",
                   url: wsm_ajaxObject.ajax_url,
                   data: { action: \'timezoneByCountry\', r: Math.random(),code:this.value }
               }).done(function( timezone ) {
                    jQuery("#'.WSM_PREFIX.'TimezoneString").val(timezone);
                   $(\'#submit\').prop(\'disabled\', false);
               });
            })
            ';
        echo $html.=$this->endWrapper;
    }
    function wsmGetTimeZoneDropDown($tzstring){
        $html='<select id="'.WSM_PREFIX.'TimezoneString" name="'.WSM_PREFIX.'TimezoneString" aria-describedby="timezone-description">'.     wp_timezone_choice( $tzstring, get_user_locale() ).'</select>';
        return $html;
    }
    function wsmGetCountryDropDown($code=''){
        $arrCountries=$this->objDatabase->fnGetAllCountries();
        $html='<select id="'.WSM_PREFIX.'Country" name="'.WSM_PREFIX.'Country" >';
        foreach($arrCountries as $country){
            $selected="";
            if($country['alpha2Code']==$code){
                $selected='selected="selected"';
            }
            $html.='<option value="'.$country['alpha2Code'].'" '.$selected.'>'.__($country['name'],WSM_PREFIX).'</option>';
        }
        return $html.='</select>';
    }
    function wsmGetArchiteDaysDropDown($days=30)
	{
		$html='<select id="'.WSM_PREFIX.'ArchiveDays" name="'.WSM_PREFIX.'ArchiveDays" >';
		if($days==30){
            $html.='<option value="30" selected="selected">'.__('Last 30 Days',WSM_PREFIX).'</option>';
        }else{
            $html.='<option value="30">'.__('Last 30 Days',WSM_PREFIX).'</option>';
        }
        if($days==60){
            $html.='<option value="60" selected="selected">'.__('Last 60 Days',WSM_PREFIX).'</option>';
        }else{
            $html.='<option value="60">'.__('Last 60 Days',WSM_PREFIX).'</option>';
        }
		$html.='</select>';
		return $html;
	}
    
    function wsmGetChartDaysDropDown($days=30){
        $html='<select id="'.WSM_PREFIX.'ChartDays" name="'.WSM_PREFIX.'ChartDays" >';
        if($days==15){
            $html.='<option value="15" selected="selected">'.__('Last 15 Days',WSM_PREFIX).'</option>';
        }else{
            $html.='<option value="15">'.__('Last 15 Days',WSM_PREFIX).'</option>';
        }
        if($days==30 || $days=='' ){
            $html.='<option value="30" selected="selected">'.__('Last 30 Days',WSM_PREFIX).'</option>';
        }else{
            $html.='<option value="30">'.__('Last 30 Days',WSM_PREFIX).'</option>';
        }
        if($days==45){
            $html.='<option value="45" selected="selected">'.__('Last 45 Days',WSM_PREFIX).'</option>';
        }else{
            $html.='<option value="45">'.__('Last 45 Days',WSM_PREFIX).'</option>';
        }
        if($days==60){
            $html.='<option value="60" selected="selected">'.__('Last 60 Days',WSM_PREFIX).'</option>';
        }else{
            $html.='<option value="60">'.__('Last 60 Days',WSM_PREFIX).'</option>';
        }
        return $html.='</select>';
    }
    function wsmCreateSubLayout($layout){
        global $wsmAdminPageHooks,$wsmRequestArray,$wp_meta_boxes;        
        switch($layout){
            case 'Summary':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-4" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'bottom', null );
                echo '</div>';
            break;
            case 'UsersOnline':
                $tab=isset($wsmRequestArray['subTab'])&&$wsmRequestArray['subTab']!=""?$wsmRequestArray['subTab']:'';
                if($tab!=''){
                    switch($tab){
                        case 'summary':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                            echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                            @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'left', null );
                            echo '</div>';
                            echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                            @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'right', null );
                            echo '</div>';
                        break;
                        case 'recent':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                        break;
                        case 'mavis':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                        break;
                        case 'popPages':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                        break;
                        case 'popReferrer':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                        break;
                        case 'geoLocation':
                            echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                            @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'top', null );
                            echo '</div>';
                            echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                            @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'left', null );
                            echo '</div>';
                            echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                            @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'right', null );
                            echo '</div>';
                        break;
                    }
                }
            break;
            case 'TrafStats':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_traffic'], 'bottom', null );
                echo '</div>';
            break;
           case 'RefSites':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_trafficsrc'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_trafficsrc'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_trafficsrc'], 'bottom', null );
                echo '</div>';  
				break;          
           case 'SearchEngines':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_trafficsrc'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_trafficsrc'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_trafficsrc'], 'bottom', null );
                echo '</div>';   
				break;          
           case 'SearchKeywords':
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_trafficsrc'], 'bottom', null );
                echo '</div>';   
				break;
           case 'bosl':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_bosl'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_bosl'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_bosl'], 'bottom', null );
                echo '</div>';            
 			    break;
           case 'GeoLocation':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_visitors'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_visitors'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_visitors'], 'bottom', null );
                echo '</div>';            
 			    break; 
           case 'byURL':
           case 'byTitle':
                echo '<div id="wsm-postbox-container-2" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_content'], 'left', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-3" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_content'], 'right', null );
                echo '</div>';
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_content'], 'bottom', null );
                echo '</div>';            
 			    break; 
           case 'ipexc':
                echo '<div id="wsm-postbox-container-1" class="postbox-container">';
                @do_meta_boxes( $wsmAdminPageHooks[WSM_PREFIX.'_ipexc'], 'bottom', null );
                echo '</div>';   
				break;
        }
    }
    function wsmShowMainPageLayout($page){
        global $wsmAdminPageHooks,$wsmAdminJavaScript,$wsmRequestArray;
        echo $this->startMetaBoxWrapper;
        echo '<form name="'.WSM_PREFIX.'mainMetboxForm" id="'.WSM_PREFIX.'mainMetboxForm" method="post">';
        wp_nonce_field( 'some-action-nonce' );        
        $subPage=isset($wsmRequestArray['subPage']) && $wsmRequestArray['subPage']!=''?$wsmRequestArray['subPage']:'bosl';
        if($subPage!=''){
            $this->wsmCreateSubLayout($subPage);
        }
        /* Used to save closed meta boxes and their order */
        wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false );
        wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false );
        echo '</form>';
        echo $this->endMetaBoxWrapper;        
        $wsmAdminJavaScript.='if(jQuery(".'.$wsmAdminPageHooks[$page].'").length){
                    postboxes.add_postbox_toggles("'.$wsmAdminPageHooks[$page].'");
                }';
    }
    function wsmViewTraffic(){
        global $wsmAdminPageHooks,$wsmAdminJavaScript,$wsmRequestArray;
        echo $this->startWrapper;
        echo $this->fnPrintTitle('Traffic');        
        $this->wsmShowMainPageLayout(WSM_PREFIX.'_traffic');
     /*   echo $html.=$this->startMetaBoxWrapper;
        echo '<form name="'.WSM_PREFIX.'mainMetboxForm" id="'.WSM_PREFIX.'mainMetboxForm" method="post">';
        wp_nonce_field( 'some-action-nonce' );        
        $subPage=isset($wsmRequestArray['subPage']) && $wsmRequestArray['subPage']!=''?$wsmRequestArray['subPage']:'';
        if($subPage!=''){
            $this->wsmCreateLayout($subPage);
        }
        /* Used to save closed meta boxes and their order 
        wp_nonce_field( 'meta-box-order', 'meta-box-order-nonce', false );
        wp_nonce_field( 'closedpostboxes', 'closedpostboxesnonce', false );

        echo '</form>';
        echo $this->endMetaBoxWrapper;
        echo $this->endWrapper;
        $wsmAdminJavaScript.='if(jQuery(".'.$wsmAdminPageHooks[WSM_PREFIX.'_traffic'].'").length){
                    postboxes.add_postbox_toggles("'.$wsmAdminPageHooks[WSM_PREFIX.'_traffic'].'");
                }';*/
        echo $this->endWrapper;
    }

    function wsmViewTrafficSources(){
        global $wsmAdminPageHooks,$wsmAdminJavaScript,$wsmRequestArray;
        echo $this->startWrapper;
        echo $this->fnPrintTitle('Traffic Sources');
        $this->wsmShowMainPageLayout(WSM_PREFIX.'_trafficsrc');
        echo $this->endWrapper;
    }
    function wsmViewVisitors(){
        echo $this->startWrapper;
        echo $this->fnPrintTitle('Visitors');
        $this->wsmShowMainPageLayout(WSM_PREFIX.'_visitors');
        echo $this->endWrapper;
    }
    function wsmViewContent(){
        echo $this->startWrapper;
        echo $this->fnPrintTitle('Content');
        $this->wsmShowMainPageLayout(WSM_PREFIX.'_content');
        echo $this->endWrapper;
    }
    function wsmViewIPExclusion(){
        echo $this->startWrapper;
        echo $this->fnPrintTitle('I.P. Exclusion');
        $this->wsmShowMainPageLayout(WSM_PREFIX.'_ipexc');
        echo $this->endWrapper;
    }
}
