<?php
//add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );
//function my_theme_enqueue_styles() {
//    wp_enqueue_style( 'travem-master', get_template_directory_uri() . '/style.css' );
//}


//Shortcode 1: Text - Found at Contact Page
function sh1_shortcode( $atts , $content = null ) 
{
	return"We would love to hear from you! Connect with us on here if you wish to make an appointment or have any queries.";
}
add_shortcode( 'shortcode1', 'sh1_shortcode');


//Shortcode 2: Video - Found at Tas-Silg Post
function sh2_shortcode($atts, $content=null) 
{
    extract(
        shortcode_atts(array(
            'site' => 'youtube',
            'id' => '',
            'w' => '600',
            'h' => '370'
        ), $atts)
    );
    if ( $site == "youtube" ) { $src = 'http://www.youtube-nocookie.com/embed/'.$id; }
    if ( $id != '' ) {
        return '<iframe width="1361" height="480" src="https://www.youtube.com/embed/IArP3eBOljA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
    }
}
add_shortcode('shortcode2', 'sh2_shortcode');


//Shortcode 3: Share to Twitter - Found at All Posts
function sh3_shortcode() 
{
  return '<div id="twitit"><a href="http://twitter.com/home?status=Currently reading '.get_permalink($post->ID).'" title="Click to send this page to Twitter!" target="_blank">Share to Twitter</a></div>';
}
add_shortcode('shortcode3', 'sh3_shortcode');


//Shortcode 4: Text - Found at About Page
function sh4_shortcode( $atts , $content = null ) 
{
	return"**All tour guides are on 'First Come First Serve' bases to every tour requested or planned by the company";
}
add_shortcode( 'shortcode4', 'sh4_shortcode' );


//Shortcode 5: iFrame - Found at Mdina Post
function sh5_shortcode($atts, $content) 
{
 if (!$atts['width']) { $atts['width'] = 800; }
 if (!$atts['height']) { $atts['height'] = 600; }

 return '<iframe border="0" class="shortcode_iframe" src="' . $atts['src'] . '" width="' . $atts['width'] . '" height="' . $atts['height'] . '"></iframe>';
}
add_shortcode('shortcode5', 'sh5_shortcode');


//Shortcode 6: Link - Found at all Posts
function sh6_shortcode($atts, $content = null) 
{
  extract(shortcode_atts(array(
    "href" => 'http://'
  ), $atts));
  return '<a href="'.$href.'">'.$content.'</a>';
}
add_shortcode("shortcode6", "sh6_shortcode");


//Shortcode 7: Image - Found at Mdina Post
function sh7_shortcode($atts, $content = null) 
{
    extract(shortcode_atts(array("src" => ''), $atts));
    return '<img src="' . $src . '" alt="'. do_shortcode($content) .'" />';
}
add_shortcode('shortcode7', 'sh7_shortcode');